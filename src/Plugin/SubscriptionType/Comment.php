<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a comment subscription type.
 *
 * @SubscriptionType(
 *   id = "comment",
 *   label = @Translation("Comment"),
 *   type = "comment",
 *   field = "cid",
 *   context_definitions = {
 *     "comment" = @ContextDefinition("entity:comment",
 *       label = @Translation("Comment"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class Comment extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'subscribe to comments';
  }

}
