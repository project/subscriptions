<?php

namespace Drupal\subscriptions\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\subscriptions\Entity\SubscriptionInterface;

/**
 * Provides a class for events for the Subscriptions queue.
 */
class SubscriptionsEvent extends Event {

  /**
   * Subscription object.
   *
   * @var EntityInterface|null
   */
  protected EntityInterface $entity;

  /**
   * The operation being performed, if provided.
   *
   * @var string|null
   */
  protected ?string $operation = NULL;

  /**
   * Create a new SubscriptionsEvent.
   *
   * @param EntityInterface|null $entity
   *   Subscription object.
   * @param string|null $operation
   *   (optional) The operation being performed. Defaults to NULL.
   */
  public function __construct(EntityInterface $entity = NULL, string $operation = NULL) {
    $this->entity = $entity;
    $this->operation = $operation;
  }

  /**
   * Get the entity subscribed to with this subscription.
   *
   * @return EntityInterface|null
   *   The subscribed entity.
   */
  public function getEntity(): ?EntityInterface {
    return $this->entity;
  }

  /**
   * Get the operation being performed, if provided.
   *
   * @return string|null
   *   The name of the operation.
   */
  public function getOperation(): ?string {
    return $this->operation;
  }

}
