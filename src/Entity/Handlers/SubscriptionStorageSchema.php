<?php

namespace Drupal\subscriptions\Entity\Handlers;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the subscription storage schema handler.
 */
class SubscriptionStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($data_table = $this->storage->getBaseTable()) {
      $schema[$data_table]['indexes'] += [
        'type__value' => ['type', 'value'],
        'recipient' => ['recipient'],
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if ($table_name == $this->storage->getBaseTable()) {
      switch ($field_name) {
        case 'recipient':
          $schema['fields'][$field_name]['not null'] = TRUE;
          break;

        case 'send_updates':
        case 'send_comments':
          $schema['fields'][$field_name]['not null'] = TRUE;
          $schema['fields'][$field_name]['default'] = 0;
          break;
      }
    }

    return $schema;
  }

}
