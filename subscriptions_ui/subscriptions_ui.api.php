<?php

/**
 * @file
 * Hooks for the Subscriptions UI module.
 */

/**
 * Declare if the module will provide the UI for a given node.
 *
 * Subscriptions UI is willing to handle all nodes. Other modules can return a
 * higher priority with their name (or a different name) depending on the NID,
 * user, etc.
 *
 * @param \Drupal\node\NodeInterface $node
 *   A node object.
 *
 * @return array|null
 *   NULL for no response, or an associative array, with the following key-value
 *   pairs:
 *   - priority: (required) Priority weight.
 *   - module: (required) Module name.
 */
function hook_subscriptions_ui(\Drupal\node\NodeInterface $node): ?array {
  return [
    'priority' => 0,
    'module' => 'subscriptions_ui',
  ];
}

/**
 * Check if Subscriptions UI is allowed to display on a given node.
 *
 * @param \Drupal\node\NodeInterface $node
 *   A node object.
 * @param string $module
 *   A module name.
 *
 * @return \Drupal\Core\Access\AccessResultInterface
 *   An access result object.
 */
function hook_subscriptions_ui_get_permission_to_handle(\Drupal\node\NodeInterface $node, string $module): \Drupal\Core\Access\AccessResultInterface {
  // Allow good_type nodes.
  if ($node->getType() == 'good_type') {
    return \Drupal\Core\Access\AccessResult::allowed();
  }
  // Forbid bad_type nodes.
  elseif ($node->getType() == 'bad_type') {
    return \Drupal\Core\Access\AccessResult::forbidden();
  }

  // Otherwise, neutral response.
  return \Drupal\Core\Access\AccessResult::neutral();
}
