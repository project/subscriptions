<?php

namespace Drupal\subscriptions\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\subscriptions\Entity\SubscriptionInterface;

/**
 * Defines an interface for subscription filter entities.
 */
interface SubscriptionFilterInterface extends ContentEntityInterface {

  /**
   * Gets the Subscription this filter references.
   *
   * @return SubscriptionInterface
   */
  public function getSubscription(): SubscriptionInterface;

  /**
   * Get the type of the subscription filter.
   *
   * @return string|null
   *   The value, or NULL if there is not a value.
   */
  public function getType(): ?string;

  /**
   * Get the value of the subscription filter.
   *
   * @return string|null
   *   The value, or NULL if there is not a value.
   */
  public function getValue(): ?string;

  /**
   * Get the recipient user.
   *
   * @return AccountInterface
   *   The user's account.
   */
  public function getRecipient(): AccountInterface;

  /**
   * Gets the subscription creation timestamp.
   *
   * @return int
   *   Creation timestamp of the subscription.
   */
  public function getCreated(): int;

  /**
   * Gets the most recent subscription modification timestamp.
   *
   * @return int
   *   Modification timestamp of the subscription.
   */
  public function getChanged(): int;

}
