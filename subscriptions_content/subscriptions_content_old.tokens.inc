<?php

/**
 * @file
 * Token callbacks for the subscriptions_content module.
 */

use Drupal\comment\Entity\Comment;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_tokens().
 */
function subscriptions_content_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  /** @var \Drupal\Core\Utility\Token $token_service */
  $token_service = \Drupal::service('token');

  if ($type == 'node' && !empty($data['node'])) {
    $node = $data['node'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the node.
        case 'node-html':
          $replacements[$original] = $node->nid;
          break;
      }
    }
  }

  if ($type == 'subs' && !empty($data['comment'])) {
    /** @var \Drupal\comment\Entity\Comment $comment */
    $comment = $data['comment'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'is-new':
          $replacements[$original] = (int) !empty($comment->_subscriptions_is_new);
          break;

        case 'is-updated':
          $replacements[$original] = (int) empty($comment->_subscriptions_is_new);
          break;

        case 'is-old':
          $replacements[$original] = 0;
          break;

        case 'is-published':
          $replacements[$original] = (int) ($comment->status == Comment::PUBLISHED);
          break;

        case 'has-distinct-summary':
          $replacements[$original] = 0;
          break;

        case 'view':
          $langcode = !empty($options['langcode']) ? $options['langcode'] : NULL;
          $element = \Drupal::entityTypeManager()->getViewBuilder('comment')->view($comment, 'subscriptions', $langcode);
          $replacements[$original] = \Drupal::service('renderer')->render($element);
          break;
      }
    }
  }
  elseif ($type == 'subs' && !empty($data['node'])) {
    $node = $data['node'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'is-new':
          $replacements[$original] = (int) !empty($node->_subscriptions_is_new);
          break;

        case 'is-updated':
          $replacements[$original] = (int) !empty($node->_subscriptions_is_updated);
          break;

        case 'is-old':
          $replacements[$original] = (int) (empty($node->_subscriptions_is_new) && empty($node->_subscriptions_is_updated));
          break;

        case 'is-published':
          $replacements[$original] = $node->status;
          break;

        case 'has-distinct-summary':
          $repls = $token_service->generate(
            'node',
            [
              'summary' => '[node:summary]',
              'body' => '[node:body]',
            ],
            $data,
            $options,
            $bubbleable_metadata,
          );
          $replacements[$original] = (int) (!empty($repls['[node:summary]']) && $repls['[node:summary]'] != $repls['[node:body]']);
          break;

        case 'view':
          $langcode = !empty($options['langcode']) ? $options['langcode'] : NULL;
          $element = \Drupal::entityTypeManager()->getViewBuilder('node')->view($node, 'subscriptions', $langcode);
          $replacements[$original] = \Drupal::service('renderer')->render($element);
          break;
      }
    }

    if (($comments_tokens = $token_service->findWithPrefix($tokens, 'comments')) && !empty($node->_subscriptions_comments)) {
      if (empty($node->_subscriptions_comments_rendered)) {
        foreach ($node->_subscriptions_comments as $comment) {
          $node->_subscriptions_comments_rendered[] = mail_edit_format($data['template']['subscriptions_comment_body'], [
            'comment' => $comment,
            'object_type' => 'comment',
          ] + $data, $options);
        }
      }
      $replacements += $token_service->generate(
        'array',
        $comments_tokens, [
          'array' => (isset($node->_subscriptions_comments_rendered) ? $node->_subscriptions_comments_rendered : []),
        ],
        ['sanitize' => FALSE] + $options,
        $bubbleable_metadata,
      );
    }
  }

  return $replacements;
}
