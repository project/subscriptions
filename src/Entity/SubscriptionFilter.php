<?php

namespace Drupal\subscriptions\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\subscriptions\Entity\SubscriptionInterface;

/**
 * Defines the SubscriptionFilter entity class.
 *
 * @ContentEntityType(
 *   id = "subscription_filter",
 *   label = @Translation("Subscription filter"),
 *   label_collection = @Translation("Subscription filters"),
 *   label_singular = @Translation("subscription filter"),
 *   label_plural = @Translation("subscription filters"),
 *   label_count = @PluralTranslation(
 *     singular = "@count subscription filter",
 *     plural = "@count subscription filters"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "storage_schema" = "Drupal\subscriptions\Entity\Handlers\SubscriptionFilterStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "subscription_filters",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "sfid",
 *     "bundle" = "type",
 *   },
 *   common_reference_target = TRUE,
 *   bundle_label = @Translation("Subscription filters type"),
 *   permission_granularity = "bundle",
 * )
 */
class SubscriptionFilter extends ContentEntityBase implements SubscriptionFilterInterface {

  /**
   * {@inheritdoc}
   */
  public function getSubscription(): SubscriptionInterface {
    return $this->get('subscription')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): ?string {
    return $this->get('value')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipient(): AccountInterface {
    return $this->get('recipient')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChanged(): int {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['subscription'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Subscription'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('target_type', 'subscription')
      ->addConstraint('NotNull');

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('max_length', 64);

    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE);

    $fields['recipient'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recipient'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('target_type', 'user')
      ->addConstraint('NotNull');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the subscription filter was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the subscription filter was last edited.'));

    return $fields;
  }

}
