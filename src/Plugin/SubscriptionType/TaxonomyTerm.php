<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a taxonomy term subscription type.
 *
 * @SubscriptionType(
 *   id = "taxonomy_term",
 *   label = @Translation("Taxonomy term"),
 *   type = "taxonomy_term",
 *   field = "tid",
 *   context_definitions = {
 *     "taxonomy_term" = @ContextDefinition("entity:taxonomy_term",
 *       label = @Translation("Taxonomy term"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class TaxonomyTerm extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'subscribe to taxonomy terms';
  }

}
