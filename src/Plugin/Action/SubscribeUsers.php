<?php

namespace Drupal\subscriptions\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Bulk subscribe users.
 *
 * @Action(
 *   id = "subscriptions_subscribe_users_action",
 *   label = @Translation("Subscribe the selected user(s)"),
 *   type = "user"
 * )
 */
class SubscribeUsers extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $account->hasPermission('bulk-administer user subscriptions');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(UserInterface $user = NULL) {
    // @todo Fill out this method stub.
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo Implement buildConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitConfigurationForm() method.
  }

}
