<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

use Drupal\Core\Access\AccessibleInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\Definition\DependentPluginDefinitionInterface;
use Drupal\entity\BundlePlugin\BundlePluginInterface;

/**
 * Provides an interface for Subscription Type plugins.
 */
interface SubscriptionTypeInterface extends AccessibleInterface, BundlePluginInterface, ContainerFactoryPluginInterface, ContextAwarePluginInterface, DependentPluginDefinitionInterface {

  /**
   * Get the plugin field.
   *
   * @return string
   *   The plugin field.
   */
  public function getField(): string;

  /**
   * Get the plugin label.
   *
   * @return string
   *   The plugin label.
   */
  public function getLabel(): string;

  /**
   * Get the plugin type.
   *
   * @return string
   *   The plugin type.
   */
  public function getType(): string;

}
