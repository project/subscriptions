<?php

namespace Drupal\subscriptions\Event;

/**
 * Provides a class for events for the Subscriptions queue.
 *
 * Declares the const strings for the events we are creating. We can dispatch
 * any of the events we define in this file by calling the Event Dispatcher and
 * passing it one of our event strings ex.
 * SubscriptionsEvents::OPERATION_INSERT and a SubscriptionsEvent object.
 *
 * Usage:
 *
 * $event = new SubscriptionsEvent(...);
 * $this->event_dispatcher->dispatch(SubscriptionsEvents::OPERATION_INSERT, $event);
 */
final class SubscriptionsEvents {

  /**
   * Event key for insertions.
   *
   * @Event
   *
   * @var string
   */
  public const OPERATION_INSERT = 'subscriptions_insert';

  /**
   * Event key for updates.
   *
   * @Event
   *
   * @var string
   */
  public const OPERATION_UPDATE = 'subscriptions_update';

  /**
   * Event key for deletions.
   *
   * @Event
   *
   * @var string
   */
  public const OPERATION_DELETE = 'subscriptions_delete';

}
