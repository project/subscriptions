Contents of this file
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers

Introduction
------------

The Subscriptions module allows users to subscribe to notifications about the
creation or updating of content, such as:

* When a specific node is created or updated
* When any node of a specific type is created or updated
* When a comment is left on a node
* When a node that is tagged with a particular taxonomy term is created or
  updates

Users can choose to auto-subscribe to their own content in order to receive
notifications about it.

For a full description of the module, visit the [project page][project_page].

To submit bug reports and feature suggestions, or track changes, visit
the [project issue queue][project_issue_queue].

Requirements
------------

This module does not depend on any modules outside of Drupal core at this time.

Recommended modules
-------------------

* [Mail Editor][mail_edit]: When enabled, the Mail Editor module lets you edit
  the body and subject of all emails that go out from your site.

Installation
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

Configuration
-------------

* Configure the user permissions in Administration » People » Permissions:

  * Use the administration pages and help

    The top-level administration categories require this permission to be
    accessible.

  * Use the toolbar

    Users with this permission will see the administration toolbar at the top of
    each page.

* Customize the module settings in Administration » Configuration » System »
  Subscriptions

@todo Continue to fill out.

Troubleshooting
---------------

@todo Fill out.

FAQ
---

@todo Fill out.

Maintainers
-----------

### Current maintainers

* Hans Salvisberg ([salvis][salvis])
* Alex McCabe ([alexdmccabe][alexdmccabe]) (sponsored by [Red Hat][red_hat])

### Drupal 7

* Hans Salvisberg ([salvis][salvis])

### Drupal 6

* Hans Salvisberg ([salvis][salvis])
* Fernando Paredes García ([develCuy][develcuy]) (sponsored
  by [University of York][university_of_york])

### Subscriptions 5.x-2.0 rewrite

* Hans Salvisberg ([salvis][salvis])
* chx ([chx][chx])

### Translations

* German: [salvis][salvis]
* Hebrew: [yhager][yhager]
* Italian: [Gregorio Magini][peterpoe]
* Spanish: [develCuy][develcuy]
* French: [matkeane][matkeane]
* Japanese: [pineray][pineray]
* Danish: [Morten Wulff][wulff] & Anders Lund (unable to locate profile)
* Brazilian Portuguese: Márcio Moreira (unable to locate profile)
* Hungarian: [muczy][muczy]
* Arabic: [N2H][N2H]

### Sponsored by

Descriptions are taken from Wikipedia's Google Knowledge panel text, generated when searching for the exact company name.

* [University of York][university_of_york]

  The URL that was provided originally was https://www.transit.york.ac.uk,
  however, that URL no longer resolves.

  The University of York is a collegiate research university, located in the
  city of York, England. Established in 1963, the university has expanded to
  more than thirty departments and centres, covering a wide range of subjects.

* [Red Hat][red_hat]

  Red Hat, Inc. is an American IBM subsidiary software company that provides
  open source software products to enterprises. Founded in 1993, Red Hat has its
  corporate headquarters in Raleigh, North Carolina, with other offices
  worldwide.

[project_page]: https://www.drupal.org/project/subscriptions

[project_issue_queue]: https://www.drupal.org/project/issues/subscriptions

[salvis]: https://www.drupal.org/user/82964

[alexdmccabe]: https://www.drupal.org/user/1358588

[mail_edit]: https://www.drupal.org/project/mail_edit

[red_hat]: https://www.redhat.com

[develcuy]: https://www.drupal.org/user/125473

[university_of_york]: https://www.york.ac.uk/

[chx]: https://www.drupal.org/user/9446

[yhager]: https://www.drupal.org/user/71425

[peterpoe]: https://www.drupal.org/user/55674

[matkeane]: https://www.drupal.org/user/198280

[pineray]: https://www.drupal.org/user/103447

[wulff]: https://www.drupal.org/user/7118

[muczy]: https://www.drupal.org/user/183453

[N2H]: https://www.drupal.org/user/346050
