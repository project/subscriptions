<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * A base class that provides a simple access check based on permissions.
 */
abstract class SubscriptionTypePermissionAccessBase extends SubscriptionTypeBase {

  /**
   * {@inheritdoc}
   */
  protected function getAccessResult(string $operation, AccountInterface $account): AccessResultInterface {
    // Get an access result based on permissions.
    $access_result = AccessResult::allowedIfHasPermission($account, $this->getPermission());
    // If the access result is neutral (no opinion), create a forbidden access
    // result and return that instead of the neutral result.
    if ($access_result->isNeutral() === TRUE) {
      // Create the new access result using the same reason response.
      $access_result = AccessResult::forbidden($access_result->getReason());
      // Add user permissions cache context.
      $access_result->cachePerPermissions();
    }

    return $access_result;
  }

  /**
   * Get the permission required for the subscription type.
   *
   * @return string
   *   The permission.
   */
  abstract protected function getPermission(): string;

}
