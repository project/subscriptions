<?php

namespace Drupal\subscriptions\Plugin\Manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\subscriptions\Plugin\Annotation\SubscriptionType;
use Drupal\subscriptions\Plugin\SubscriptionType\SubscriptionTypeInterface;

/**
 * Provides a Subscription Type plugin manager.
 */
class SubscriptionTypeManager extends DefaultPluginManager {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   A cache backend service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdirectory = 'Plugin/SubscriptionType';
    $plugin_interface = SubscriptionTypeInterface::class;
    $plugin_definition_annotation_name = SubscriptionType::class;

    parent::__construct(
      $subdirectory,
      $namespaces,
      $module_handler,
      $plugin_interface,
      $plugin_definition_annotation_name,
    );

    $this->alterInfo('subscription_type_plugin');
    $this->setCacheBackend($cache_backend, 'subscription_type_plugin', ['subscription_type_plugin']);
  }

}
