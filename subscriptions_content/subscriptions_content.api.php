<?php

/**
 * @file
 * Hooks for the Subscriptions Content module.
 */

/**
 * Provide options for node subscription forms.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The current user account.
 * @param \Drupal\node\NodeInterface $node
 *   The node.
 *
 * @return array
 *   An associative array of option information. The keys are fields (e.g., nid,
 *   tid), and each value is an associative array with the following keys:
 *   - name: (required) The human-readable label of the option.
 *   - params: (required) An associative array with the following keys:
 *     - module: (required) The name of the module.
 *     - field: (required) The name of the field.
 *     - value: (required) The value of the field that is being subscribed to.
 *   - link: (deprecated) An unused value.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 * @ingroup subscriptions_content
 */
function hook_subscriptions_content_node_options(\Drupal\Core\Session\AccountInterface $account, \Drupal\node\NodeInterface $node): array {
  $options = [];

  $options['eid'] = [
    'name' => t('Subscribe to this example'),
    'params' => [
      'module' => 'node',
      'field' => 'nid',
      'value' => $node->id(),
    ],
    'link' => 'example/' . $node->id(),
  ];

  return $options;
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 * @ingroup subscriptions_content
 */
function hook_subscriptions_content_node_options_alter() {
}
