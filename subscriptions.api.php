<?php

/**
 * @file
 * Hooks for the subscriptions module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Check access.
 *
 * @param mixed $load_function
 *   Unknown.
 * @param mixed $load_args
 *   Unknown.
 * @param mixed $object
 *   Object such as a node.
 *
 * @return int
 *   1 if access should be allowed, 0 otherwise.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_access($load_function, $load_args, $object): int {
  if (\Drupal::currentUser()->isAuthenticated() && $object != NULL) {
    return 1;
  }
  else {
    return 0;
  }
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_access_alter() {
}

/**
 * Alter subscription counts.
 *
 * @param array $counts
 *   An array indexed by subscription type, containing the counts.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user whose counts are being collected.
 */
function hook_subscriptions_counts_alter(array &$counts, \Drupal\Core\Session\AccountInterface $account) {
  $counts['example']['eid'] += $account->id();
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_data_alter() {
}

/**
 * Deprecated.
 *
 * @see \hook_subscriptions_types()
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_fields() {
  return [];
}

/**
 * Deprecated.
 *
 * Create and dispatch a QueueEvent instead.
 *
 * @see \Drupal\subscriptions\Event\QueueEvent
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_queue() {
  /** @var \Drupal\node\NodeInterface|\stdClass $node */
  $node = new stdClass();
  $inserted = TRUE;

  // Drupal 7:
  $event = [
    'module' => 'node',
    'uid' => $node->uid,
    'load_function' => 'subscriptions_content_load_node',
    'load_args' => $node->nid,
    'type' => 'node',
    'action' => ($inserted ? 'insert' : 'update'),
    'is_new' => $inserted,
    'node' => $node,
  ];
  subscriptions_queue($event);

  // Drupal 9:
  $event = new \Drupal\subscriptions\Event\QueueEvent(
    'node',
    'nid',
    $node->id(),
    $inserted ? \Drupal\subscriptions\Event\QueueEvent::OPERATION_INSERT : \Drupal\subscriptions\Event\QueueEvent::OPERATION_UPDATE,
    $node->getOwnerId(),
  );
  /** @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $event_dispatcher */
  \Drupal::service('event_dispatcher')->dispatch($event);
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_queue_query_alter() {
}

/**
 * Deprecated.
 *
 * @deprecated.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_stype() {
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @see hook_subscriptions_stype()
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_stype_alter() {
}

/**
 * This hook does not seem to be invoked anywhere.
 *
 * @see hook_subscriptions_stype()
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_stypes() {
}

/**
 * Returns the available token types.
 *
 * @param string $mailkey
 *   A mailkey.
 * @param array $options
 *   (optional) Options.
 *
 * @return array
 *   An array of token types.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_token_types(string $mailkey, array $options = []): array {
  $token_types = [];

  if ($mailkey == 'example-type-') {
    $token_types[] = 'example';
    $token_types[] = 'comment';
  }

  return $token_types;
}

/**
 * Define subscription types.
 *
 * @return array
 *   An array of subscription type definitions. Each definition is keyed by a
 *   unique identifier. The corresponding array describing the subscription type
 *   may contain the following key-value pairs:
 *   - title: The title of the subscription type.
 *   - type: The subscription type.
 *   - access: Conditions that must be met to grant access. All conditions must
 *     be met to gain access. The conditions are an array of one or more of the
 *     following:
 *     - permission: A permission string.
 *     - access: A callable that returns an implementation of
 *       AccessResultInterface.
 *
 * @see \Drupal\Core\Access\AccessResultInterface
 * @see user_api
 * @see callbacks
 * @see https://www.php.net/manual/en/language.types.callable.php
 * @see \callback_subscriptions_example_access()
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_types(): array {
  $subscription_types = [];

  // Subscriptions to Examples by ID.
  $subscription_types['example'] = [
    'title' => t('Examples'),
    'type' => 'example',
    'access' => [
      'permission' => 'subscribe to content',
    ],
    'data_function' => 'subscriptions_example_data',
    'mailkey' => 'example-type-',
  ];

  // Subscriptions to Examples by bundle, with a custom access callback.
  $subscription_types['example_type'] = [
    'title' => t('Example types'),
    'type' => 'example',
    // Use a custom access callback. The callback will get the current user
    // object and the subscription type information as arguments, and must
    // return an implementation of \Drupal\Core\Access\AccessResultInterface.
    // @see callbacks
    // @see https://www.php.net/manual/en/language.types.callable.php
    'access' => [
      'callback' => 'example_subscriptions_example_type_access',
    ],
    'data_function' => 'subscriptions_example_data',
    'mailkey' => 'example-type-',
  ];

  return $subscription_types;
}

/**
 * Access callback for a subscription type.
 *
 * @param \Drupal\Core\Session\AccountInterface $user
 *   The current user account.
 * @param array $subscription_type
 *   The subscription type information.
 *
 * @see hook_subscriptions_types()
 * @see \Drupal\Core\Access\AccessResultInterface
 * @see callbacks
 * @see user_api
 *
 * @ingroup callbacks
 * @ingroup subscriptions
 */
function callback_subscriptions_example_access(\Drupal\Core\Session\AccountInterface $user, array $subscription_type): \Drupal\Core\Access\AccessResultInterface {
  // Allow access if the user's ID is an odd number.
  if ($user->id() % 2 === 1) {
    return \Drupal\Core\Access\AccessResult::allowed();
  }
  // Deny access if the user has the letter Y anywhere in their account name.
  elseif (mb_stripos($user->getAccountName(), 'Y') !== FALSE) {
    return \Drupal\Core\Access\AccessResult::forbidden();
  }
  // If nothing else, return a neutral result.
  else {
    return \Drupal\Core\Access\AccessResult::neutral();
  }
}

/**
 * Data callback for subscription types.
 *
 * @param array $data
 *   Array of data to pass to drupal_mail() and ultimately hook_tokens().
 * @param object|array $item
 *   Object used to fill $mailvars.
 * @param array $queue_item
 *   Subscriptions queue item.
 *
 * @see \hook_subscriptions_types()
 * @see subscriptions_data()
 * @see callbacks
 *
 * @ingroup callbacks
 * @ingroup subscriptions
 */
function callback_subscriptions_example_data(array &$data, $item, array $queue_item) {
  $data['example'] = (is_array($item) ? (object) $item : $item);
  subscriptions_data($data, $queue_item);
}

/**
 * This alter hook does not seem to be implemented anywhere.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 */
function hook_subscriptions_types_alter() {
}

/**
 * @} End of "addtogroup hooks".
 */
