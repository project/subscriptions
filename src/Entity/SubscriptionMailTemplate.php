<?php

namespace Drupal\subscription\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\subscription\Entity\SubscriptionMailTemplateInterface;

/**
 * Defines the subscription mail template entity.
 *
 * @ingroup subscription
 *
 * @ContentEntityType(
 *   id = "subscription_mail_template",
 *   label = @Translation("Subscription mail template"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "subscription_mail_template",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class SubscriptionMailTemplate extends ContentEntityBase implements SubscriptionMailTemplateInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Subscription mail template entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Subscription mail template entity.'))
      ->setReadOnly(TRUE);

    $fields['mail_template_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mail template ID'))
      ->setDescription(t('The ID of this mail template. This ID is returned from One Platform in response to creating a new mail template.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailTemplateId(): string {
    return $this->get('mail_template_id')->value;
  }

}
