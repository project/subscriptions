<?php

/**
 * @file
 * Subscriptions module installation.
 */

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_schema().
 */
function subscriptions_schema() {
  $schema['subscriptions_queue'] = [
    'fields' => [
      'sqid' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'uid' => [
        'type' => 'int',
        'not null' => FALSE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => '60',
        'not null' => FALSE,
      ],
      'language' => [
        'type' => 'varchar',
        'length' => '12',
        'not null' => FALSE,
      ],
      'type' => [
        'type' => 'varchar',
        'length' => '64',
        'not null' => FALSE,
      ],
      'value' => [
        'type' => 'varchar',
        'length' => '237',
        'not null' => FALSE,
      ],
      'author_uid' => [
        'type' => 'int',
        'not null' => FALSE,
      ],
      'send_interval' => [
        'type' => 'int',
        'not null' => FALSE,
      ],
      'digest' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ],
      'load_args' => [
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ],
      'load_function' => [
        'type' => 'varchar',
        'length' => '60',
        'not null' => TRUE,
        'default' => '',
      ],
      'is_new' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => FALSE,
      ],
      'last_sent' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'suspended' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['sqid'],
    'indexes' => [
      'load_args' => [
        'load_args',
        'load_function',
        'uid',
      ],
      'uid' => ['uid'],
    ],
  ];

  $schema['subscriptions_user'] = [
    'fields' => [
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'digest' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'secure_links' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'send_interval' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ],
      'send_updates' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'send_comments' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'send_interval_visible' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'send_updates_visible' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'send_comments_visible' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'autosub_on_post' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'autosub_on_update' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'autosub_on_comment' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
      'send_self' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'suspended' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['uid'],
  ];

  $schema['subscriptions_last_sent'] = [
    'fields' => [
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
      ],
      'send_interval' => [
        'type' => 'int',
        'not null' => TRUE,
      ],
      'last_sent' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => [
      'uid',
      'send_interval',
    ],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function subscriptions_install() {
  \Drupal::database()->insert('subscriptions_user')
    ->fields(['uid'], [0])
    ->execute();
  $select = \Drupal::database()->select('users', 'u')
    ->fields('u', ['uid'])
    ->condition('u.uid', 0, '>');
  \Drupal::database()->insert('subscriptions_user')
    ->from($select)
    ->execute();
}

/**
 * Implements hook_update_last_removed().
 */
function subscriptions_update_last_removed() {
  return 7101;
}

/**
 * Implements hook_update_N().
 */
function subscriptions_update_9001() {
  \Drupal::entityDefinitionUpdateManager()->installEntityType(new ContentEntityType([
    'id' => 'subscription_filter',
    'label' => new TranslatableMarkup('Subscription filter'),
    'label_collection' => new TranslatableMarkup('Subscription filters'),
    'label_singular' => new TranslatableMarkup('subscription filter'),
    'label_plural' => new TranslatableMarkup('subscription filters'),
    'handlers' => [
      'storage' => 'Drupal\Core\Entity\Sql\SqlContentEntityStorage',
      'storage_schema' => 'Drupal\subscriptions\Entity\Handlers\SubscriptionFilterStorageSchema',
      'view_builder' => 'Drupal\Core\Entity\EntityViewBuilder',
      'access' => 'Drupal\Core\Entity\EntityAccessControlHandler',
      'views_data' => 'Drupal\views\EntityViewsData',
      'list_builder' => 'Drupal\Core\Entity\EntityListBuilder',
    ],
    'base_table' => 'subscription_filters',
    'translatable' => FALSE,
    'entity_keys' => [
      'id' => 'sfid',
      'bundle' => 'type',
    ],
    'common_reference_target' => TRUE,
    'bundle_label' => new TranslatableMarkup('Subscription filters type'),
    'permission_granularity' => "bundle",
  ]));
}
