<?php

namespace Drupal\subscription\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines an interface for subscription mail templates.
 */
interface SubscriptionMailTemplateInterface extends ContentEntityInterface {

  /**
   * Returns the mail template id.
   *
   * @return string
   *   The mail template ID string.
   */
  public function getMailTemplateId(): string;

}
