<?php

namespace Drupal\subscriptions_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Subscriptions UI form.
 */
class SubscriptionsUINodeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriptions_ui_node_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $node = $this->getRouteMatch()->getParameter('node');
    $node_type = $node->type->entity->label();
    $config = $this->config('subscriptions_ui.settings');
    $expand = $config->get('subscriptions_form_expanded');

    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => 'Subscribe',
      '#collapsible' => TRUE,
      '#collapsed' => !$expand,
      '#attributes' => ['id' => 'subscribe'],
    ];

    $form['wrapper']['table'] = [
      '#type' => 'tableselect',
      '#header' => [
        'subscription_option' => $this->t(''),
      ],
      '#options' => [
        'this_page' => [
          'subscription_option' => $this->t('Subscribe to this page')->render(),
        ],
        'node_type' => [
          'subscription_option' => $this->t('Subscribe to @node_type content', ['@node_type' => $node_type])->render(),
        ],
      ],
      '#empty' => t('No content has been found.'),
      '#js_select' => FALSE,
    ];

    $form['wrapper']['actions'] = [
      '#type' => 'actions',
    ];
    $form['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo We need to modify this submit handler & make it work for the 2.0.x
    // I just copy/pasted over the submit hook from the 7.x version and put it
    // here for now.
    $recipient_uid = $form_state['values']['account']->uid;
    $default_send_intervals = $form_state['values']['send_intervals'];

    foreach ($form_state['values']['subscriptions'] as $index => $value) {
      $params = $form_state['values']['params'][$index];
      $args = [
        $params['module'],
        $params['field'],
        $params['value'],
        $params['author_uid'],
        $recipient_uid,
      ];
      if ($value) {
        $args[] = $default_send_intervals[$index];
        $args[] = !empty($form_state['values']['updates'][$index]);
        $args[] = !empty($form_state['values']['comments'][$index]);
        call_user_func_array('subscriptions_write_subscription', $args);
      }
      else {
        subscriptions_delete($args['4'], $args['0'], $args['1'], $args['2'], $args['3']);
      }
    }
    $form_state->setRedirect(str_replace('/subscribe', '', $this->getRouteMatch()->getRouteObject()->getPath()));
    $this->messenger()->addStatus($this->t('Successfully subscribed.'));
  }

}
