<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a language/translation subscription type.
 *
 * @SubscriptionType(
 *   id = "language",
 *   label = @Translation("Language"),
 *   type = "node",
 *   field = "langcode",
 *   context_definitions = {
 *     "language" = @ContextDefinition("entity:node",
 *       label = @Translation("Language"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class Language extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'subscribe to translated content';
  }

}
