<?php

namespace Drupal\subscriptions\Plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Subscription Type annotation object.
 *
 * @Annotation
 */
class SubscriptionType extends Plugin {

}
