<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\Definition\DependentPluginDefinitionTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginDependencyTrait;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for Subscription Type plugins.
 */
abstract class SubscriptionTypeBase extends PluginBase implements SubscriptionTypeInterface {

  use ContextAwarePluginTrait;
  use DependentPluginDefinitionTrait;
  use PluginDependencyTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
    );
  }

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   *
   * Overridden by adding the final keyword and passing the operation and
   * account arguments to an abstract function that subclasses must implement.
   *
   * @see \Drupal\subscriptions\Plugin\SubscriptionType\SubscriptionTypeBase::getAccessResult()
   */
  final public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    // Default to the current user.
    if (empty($account)) {
      $account = $this->currentUser;
    }

    // Reject anonymous users.
    if ($account->isAnonymous()) {
      $access_result = AccessResult::forbidden('Anonymous users cannot interact with subscriptions.');
    }
    // If the user is not anonymous, pass the operation and account to the
    // subclass implementation of getAccessResult().
    else {
      $access_result = $this->getAccessResult($operation, $account);
    }

    // Return either the access result or a boolean as necessary.
    if ($return_as_object == TRUE) {
      return $access_result;
    }
    else {
      return $access_result->isAllowed();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions(): array {
    $fields = [];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getField(): string {
    return $this->pluginDefinition['field'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->pluginDefinition['type'];
  }

  /**
   * Checks access.
   *
   * @param string $operation
   *   The operation to be performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result object.
   *
   * @see \Drupal\subscriptions\Plugin\SubscriptionType\SubscriptionTypeBase::access()
   */
  abstract protected function getAccessResult(string $operation, AccountInterface $account): AccessResultInterface;

}
