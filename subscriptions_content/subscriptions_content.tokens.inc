<?php

/**
 * @file
 * Token callbacks for the subscriptions_content module.
 */

use Drupal\comment\Entity\Comment;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function subscriptions_content_token_info() {
  $types = [];
  $types['subscriptions_content'] = [
    'name' => t('Subscription Content Tokens'),
    'description' => t('Tokens related to the subscription content.'),
  ];

  $tokens = [];
  // Entity:node rules.
  $tokens['subscriptions_content']['node-html'] = [
    'name' => 'Node HTML',
    'description' => 'Node content replacement token. Delivers a node id.',
    'needs-data' => 'entity'
  ];

  // Subscriptions Comment/Node tokens.
  $tokens['subscriptions_content']['is-new'] = [
    'name' => 'Comment/Node is-new',
    'description' => 'Comment/Node was just posted.',
    'needs-data' => 'subs'
  ];
  $tokens['subscriptions_content']['is-updated'] = [
    'name' => 'Comment/Node is-updated',
    'description' => 'Comment/Node was just updated.',
    'needs-data' => 'subs'
  ];
  $tokens['subscriptions_content']['is-published'] = [
    'name' => 'Comment/Node is-published',
    'description' => 'Comment/Node was just published.',
    'needs-data' => 'subs'
  ];
  $tokens['subscriptions_content']['has-distinct-summary'] = [
    'name' => 'Comment/Node has-distinct-summary',
    'description' => 'Comment/Node has a distinct summary value.',
    'needs-data' => 'subs'
  ];
  $tokens['subscriptions_content']['view'] = [
    'name' => 'Comment/Node view',
    'description' => 'Comment/Node view was supplied.',
    'needs-data' => 'subs'
  ];

  $types['category'] = array(
    'name' => "Category",
    'description' => "The category (taxonomy term), if this notification was triggered by a Category subscription.",
    'type' => 'term',
  );

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}


/**
 * Implements hook_tokens().
 */
function subscriptions_content_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  /** @var \Drupal\Core\Utility\Token $token_service */
  $token_service = \Drupal::service('token');

  // Perform actions on different token types.
  switch ($type) {
    case "node":
      // Node tweaks.
      if (!empty($data['node'])){
        /** @var \Drupal\Core\Entity\ContentEntityInterface $node */
        $node = $data['node'];
        $nid = $node->id();

        // Check all tokens for node-html.
        foreach ($tokens as $name => $original) {
          switch ($name) {
            // Simple key values on the node.
            case 'node-html':
              $replacements[$original] = $nid;
              break;
          }
        }
      }
      break;

    case 'subs':
      if (!empty($data['comment'])) {
        // Subscription Comment tweaks.
        /** @var \Drupal\comment\Entity\Comment $comment */
        $comment = $data['comment'];
        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'is-new':
              $replacements[$original] = (int) !empty($comment->_subscriptions_is_new);
              break;

            case 'is-updated':
              $replacements[$original] = (int) empty($comment->_subscriptions_is_new);
              break;

            case 'is-old':
              $replacements[$original] = 0;
              break;

            case 'is-published':
              $replacements[$original] = (int) ($comment->status == Comment::PUBLISHED);
              break;

            case 'has-distinct-summary':
              $replacements[$original] = 0;
              break;

            case 'view':
              $langcode = !empty($options['langcode']) ? $options['langcode'] : NULL;
              $element = \Drupal::entityTypeManager()->getViewBuilder('comment')->view($comment, 'subscriptions', $langcode);
              $replacements[$original] = \Drupal::service('renderer')->render($element);
              break;
          }
        }
      }
      elseif(!empty($data['entity_type']) && $data['entity_type'] == 'node') {
        // Subscription node tweaks.
        /** @var \Drupal\Core\Entity\ContentEntityInterface $node */
        $node = $data['node'];

        foreach ($tokens as $name => $original) {
          switch ($name) {
            case 'is-new':
              $replacements[$original] = (int) !empty($node->_subscriptions_is_new);
              break;

            case 'is-updated':
              $replacements[$original] = (int) !empty($node->_subscriptions_is_updated);
              break;

            case 'is-old':
              $replacements[$original] = (int) (empty($node->_subscriptions_is_new) && empty($node->_subscriptions_is_updated));
              break;

            case 'is-published':
              $replacements[$original] = $node->status;
              break;

            case 'has-distinct-summary':
              $repls = $token_service->generate(
                'node',
                [
                  'summary' => '[node:summary]',
                  'body' => '[node:body]',
                ],
                $data,
                $options,
                $bubbleable_metadata,
              );
              $replacements[$original] = (int) (!empty($repls['[node:summary]']) && $repls['[node:summary]'] != $repls['[node:body]']);
              break;

            case 'view':
              $langcode = !empty($options['langcode']) ? $options['langcode'] : NULL;
              $element = \Drupal::entityTypeManager()
                ->getViewBuilder('node')
                ->view($node, 'subscriptions', $langcode);
              $replacements[$original] = \Drupal::service('renderer')
                ->render($element);
              break;
          }
        }
      }
      else if (!empty($data['subs']['category'])) {
        if (isset($options['language'])) {
          $language_code = $options['language']->language;
        }
        else {
          $language_code = NULL;
        }
        // Add in taxonomy Category.
        $term = $data['subs']['category'];

        /** @var \Drupal\Core\Render $rendered */
        $rendered = \Drupal::service('renderer');

        foreach ($tokens as $name => $original) {
          switch ($name) {
            // Default values for the chained tokens handled below.
            case 'category':
              $replacements[$original] = $rendered->renderPlain(taxonomy_term_view($term, 'full', $language_code));
              break;
          }
        }

        if ($term_tokens = token_find_with_prefix($tokens, 'category')) {
          $replacements += token_generate('term', $term_tokens, ['term' => $term], $options);
        }
      }
      break;
  }

  // Fetch Specific Comment Tokens.
  $comments_tokens = $token_service->findWithPrefix($tokens, 'comments');
  if (!empty($comments_tokens) && !empty($node->_subscriptions_comments)) {
    if (empty($node->_subscriptions_comments_rendered)) {
      foreach ($node->_subscriptions_comments as $comment) {
        $node->_subscriptions_comments_rendered[] = mail_edit_format($data['template']['subscriptions_comment_body'], [
            'comment' => $comment,
            'object_type' => 'comment',
          ] + $data, $options);
      }
    }
    $replacements += $token_service->generate(
      'array',
      $comments_tokens, [
      'array' => (isset($node->_subscriptions_comments_rendered) ? $node->_subscriptions_comments_rendered : []),
    ],
      ['sanitize' => FALSE] + $options,
      $bubbleable_metadata,
    );
  }

  return $replacements;
}
