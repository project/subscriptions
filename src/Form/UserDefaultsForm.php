<?php

namespace Drupal\subscriptions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a settings form for user defaults.
 */
class UserDefaultsForm extends ConfigFormBase {

  /**
   * The database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('module_handler'),
      $container->get('current_user'),
    );
  }

  /**
   * UserDefaultsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The current user.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Connection $database,
    ModuleHandlerInterface $module_handler,
    AccountProxyInterface $user
  ) {
    parent::__construct($config_factory);

    $this->database = $database;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['subscriptions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriptions_user_defaults';
  }

  /**
   * {@inheritdoc}
   *
   * @todo Finish this up once subscriptions_content is ported.
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $query = $this->database->select('subscription')
      ->fields('subscription', ['type'])
      ->condition('recipient', !empty($user) ? $user->id() : 0)
      ->groupBy('type');
    $query->addExpression('COUNT(sid)', 'number');
    $result = $query->execute();

    $count = [];
    while ($row = $result->fetchAssoc()) {
      if (!empty($row['type'])) {
        $type = $row['type'];
        $number = $row['number'];
        $count[] = [
          $type,
          $number,
        ];
      }
    }

    $this->moduleHandler->alter('subscriptions_counts', $count);

    $header = [
      [
        'data' => $this->t('Type'),
      ],
      [
        'data' => $this->t('Number'),
      ],
    ];

    $form['overview']['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $count,
      '#empty' => $this->t('No subscriptions found.'),
      '#sticky' => TRUE,
    ];
    $form['overview']['note'] = [
      '#type' => 'item',
      '#description' => $this->t('Note: The counts on this page may differ from the ones on the detail pages for various technical reasons.'),
    ];

    return parent::buildForm($form, $form_state);
  }

}
