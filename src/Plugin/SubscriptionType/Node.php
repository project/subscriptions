<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a node subscription type.
 *
 * @SubscriptionType(
 *   id = "node",
 *   label = @Translation("Content"),
 *   type = "node",
 *   field = "nid",
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node",
 *       label = @Translation("Node"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class Node extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'subscribe to content';
  }

}
