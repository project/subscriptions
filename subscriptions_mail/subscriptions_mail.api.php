<?php

/**
 * @file
 * Hooks for the Subscriptions Mail module.
 */

/**
 * Alter a mailkey.
 *
 * @param string $mailkey
 *   The mailkey.
 * @param mixed $item
 *   The subscribed item.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 * @ingroup subscriptions_mail
 */
function hook_subscriptions_mail_mailkey_alter(string &$mailkey, $item) {
  if ($mailkey == 'example-type-') {
    return $mailkey . $item->type;
  }
}

/**
 * Get mailkeys.
 *
 * @return array
 *   An associative array, where the keys are mailkeys, and the values are
 *   translatable strings describing the mailkey.
 *
 * @ingroup hooks
 * @ingroup subscriptions
 * @ingroup subscriptions_mail
 */
function hook_subscriptions_mail_mailkeys(): array {
  $mailkeys = [];

  foreach (\Drupal\node\Entity\NodeType::loadMultiple() as $key => $type) {
    $mailkeys['node-type-' . $key] = t('Notifications for %type !content_type subscriptions', [
      '%type' => $type->label(),
      '!content_type' => t('content type'),
    ]);
  }

  return $mailkeys;
}
