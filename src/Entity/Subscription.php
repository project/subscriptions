<?php

namespace Drupal\subscriptions\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the Subscription entity class.
 *
 * @ContentEntityType(
 *   id = "subscription",
 *   label = @Translation("Subscription"),
 *   label_collection = @Translation("Subscriptions"),
 *   label_singular = @Translation("subscription"),
 *   label_plural = @Translation("subscriptions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count subscription",
 *     plural = "@count subscriptions"
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "storage_schema" = "Drupal\subscriptions\Entity\Handlers\SubscriptionStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "subscription",
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "sid",
 *     "bundle" = "type",
 *   },
 *   common_reference_target = TRUE,
 *   bundle_plugin_type = "subscriptions.subscription_type",
 *   bundle_label = @Translation("Subscription type"),
 *   permission_granularity = "bundle",
 * )
 */
class Subscription extends ContentEntityBase implements SubscriptionInterface {

  /**
   * {@inheritdoc}
   */
  public function getType(): ?string {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): ?string {
    return $this->get('value')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipient(): AccountInterface {
    return $this->get('recipient')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendInterval(): ?int {
    return $this->get('send_interval')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthor(): ?AccountInterface {
    return $this->get('author')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasAuthor(): bool {
    return (bool) $this->get('author')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendUpdates(): bool {
    return (bool) $this->get('send_updates')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSendComments(): bool {
    return (bool) $this->get('send_comments')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChanged(): int {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscribedEntity(): ?EntityInterface {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
      $storage = $this
        ->entityTypeManager()
        ->getStorage($this->getType());
    }
    catch (PluginNotFoundException $exception) {
      throw new PluginNotFoundException($exception->getMessage(), $exception->getCode(), $exception);
    }
    catch (InvalidPluginDefinitionException $exception) {
      throw new InvalidPluginDefinitionException($exception->getMessage(), $exception->getCode(), $exception);
    }

    return $storage->load($this->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): ?array {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
      $storage = $this
        ->entityTypeManager()
        ->getStorage('subscription_filter');
    }
    catch (PluginNotFoundException $exception) {
      throw new PluginNotFoundException($exception->getMessage(), $exception->getCode(), $exception);
    }
    catch (InvalidPluginDefinitionException $exception) {
      throw new InvalidPluginDefinitionException($exception->getMessage(), $exception->getCode(), $exception);
    }

    return $storage->loadByProperties(['subscription' => $this->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('max_length', 64);

    $fields['value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE);

    $fields['recipient'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Recipient'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('target_type', 'user')
      ->addConstraint('NotNull');

    $fields['send_interval'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Send interval'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE);

    $fields['author'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author UID'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setSetting('target_type', 'user');

    $fields['send_updates'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Send updates'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(FALSE)
      ->addConstraint('NotNull');

    $fields['send_comments'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Send comments'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDefaultValue(FALSE)
      ->addConstraint('NotNull');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the subscription was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the subscription was last edited.'));

    return $fields;
  }

}
