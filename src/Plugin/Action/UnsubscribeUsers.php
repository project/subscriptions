<?php

namespace Drupal\subscriptions\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Bulk unsubscribe users.
 *
 * @Action(
 *   id = "subscriptions_unsubscribe_users_action",
 *   label = @Translation("Unsubscribe the selected user(s)"),
 *   type = "user"
 * )
 */
class UnsubscribeUsers extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $account->hasPermission('bulk-administer user subscriptions');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(UserInterface $user = NULL) {
    // @todo Fill out this method stub.
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo Implement buildConfigurationForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitConfigurationForm() method.
  }

}
