<?php

namespace Drupal\subscriptions\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an interface for subscription entities.
 */
interface SubscriptionInterface extends ContentEntityInterface {

  /**
   * Get the type of the subscription.
   *
   * @return string|null
   *   The value, or NULL if there is not a value.
   */
  public function getType(): ?string;

  /**
   * Get the value of the subscription.
   *
   * @return string|null
   *   The value, or NULL if there is not a value.
   */
  public function getValue(): ?string;

  /**
   * Get the recipient user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The user's account.
   */
  public function getRecipient(): AccountInterface;

  /**
   * Get send interval value.
   *
   * @return int|null
   *   The number of seconds that must pass before a new notification can be
   *   triggered from this subscription.
   */
  public function getSendInterval(): ?int;

  /**
   * Get the author user.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The author user, or NULL if there is no author.
   */
  public function getAuthor(): ?AccountInterface;

  /**
   * Check if there is an author.
   *
   * @return bool
   *   TRUE if there is an author, FALSE otherwise.
   */
  public function hasAuthor(): bool;

  /**
   * Get send updates.
   *
   * @return bool
   *   TRUE if updates should be sent, FALSE otherwise.
   */
  public function getSendUpdates(): bool;

  /**
   * Get send comments.
   *
   * @return bool
   *   TRUE if comments should be sent, FALSE otherwise.
   */
  public function getSendComments(): bool;

  /**
   * Gets the subscription creation timestamp.
   *
   * @return int
   *   Creation timestamp of the subscription.
   */
  public function getCreated(): int;

  /**
   * Gets the most recent subscription modification timestamp.
   *
   * @return int
   *   Modification timestamp of the subscription.
   */
  public function getChanged(): int;

  /**
   * Gets the entity subscribed to with this Subscription.
   *
   * @return EntityInterface|null
   * @throws PluginNotFoundException
   * @throws InvalidPluginDefinitionException
   * @return EntityInterface|null
   *   The entity subscribed to.
   */
  public function getSubscribedEntity(): ?EntityInterface;

  /**
   * Gets all the filters, if any, for this subscription.
   *
   * @return SubscriptionFilterInterface[]|null
   *   Returns an array of subscription filters or, if there are no filters,
   *   returns null.
   */
  public function getFilters(): ?array;

}
