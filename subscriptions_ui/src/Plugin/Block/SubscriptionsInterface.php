<?php

namespace Drupal\subscriptions_ui\Plugin\Block;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\user\Plugin\views\filter\Current;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SubscriptionsInterface' block.
 *
 * @Block(
 *  id = "subscriptions_interface",
 *  admin_label = @Translation("Subscriptions interface"),
 *  context_definitions = {
 *    "node" = @ContextDefinition(
 *      "entity:node",
 *      label = @Translation("Node"),
 *      required = FALSE
 *    )
 *  }
 * )
 */
class SubscriptionsInterface extends BlockBase implements ContainerFactoryPluginInterface {
  use LoggerChannelTrait;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * FormBuilder.
   *
   * @var Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Module handler.
   *
   * @var Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * RouteMatch.
   *
   * @var Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param Drupal\Core\Config\ConfigFactory $config_factory
   *   ConfigFactory.
   * @param Drupal\Core\Session\AccountProxy $current_user
   *   Current user.
   * @param Drupal\Core\Form\FormBuilder $form_builder
   *   FormBuilder.
   * @param Drupal\Core\Extension\ModuleHandler $module_handler
   *  Module handler.
   * @param Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   RouteMatch.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactory $config_factory, AccountProxy $current_user, FormBuilder $form_builder, ModuleHandler $module_handler, CurrentRouteMatch $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->formBuilder = $form_builder;
    $this->moduleHandler = $module_handler;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('form_builder'),
      $container->get('module_handler'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Get the current Node from our block context.
    try {
      /** @var $node NodeInterface */
      $node = $this->getContextValue('node');
    }
    catch (ContextException $e) {
      $this->getLogger('subscriptions_ui')->error('Could not load node from block context.');
      return [];
    }
    // If some other module will provide the UI, return an empty array.
    if (!$this->canSubscribe($node)) {
      return [];
    }
    // Get the Subscriptions UI config values we need.
    $ui_config = $this->configFactory->get('subscriptions_ui.settings');
    $content_config = $this->configFactory->get('subscriptions_content.settings');
    $link_only = $ui_config->get('link_only');
    /** @var boolean $avoid_empty_subscribe_links */
    $avoid_empty_subscribe_links = $content_config->get('subscriptions_avoid_empty_subscribe_links');
    /** @var $blocked_content_types array */
    $blocked_content_types = $content_config->get('blocked_content_types');

    if (!$link_only) {
      return $this->formBuilder
        ->getForm('Drupal\subscriptions_ui\Form\SubscriptionsUINodeForm');
    }

    // @todo I don't know what the 2.0.x version of subscriptions_suspended is.
    // subscriptions_suspended($this->currentUser->uid, TRUE);
    if (!$avoid_empty_subscribe_links || $this->moduleHandler->invokeAll('subscriptions', 'node_options', $this->currentUser, $node)) {
      $node_type_blocked = in_array($node->type, $blocked_content_types);
      if ($node_type_blocked && !$this->currentUser->hasPermission('subscribe to all content types')) {
        return [];
      }
      // @todo I think that it would be better to update this to use the route
      // in the #url value once it is defined in the Subscriptions module.
      $uri = 'internal:/node/' . $node->id() . '/subscribe';
      $title = $this->t('Subscribe') . ($node_type_blocked ? SUBSCRIPTIONS_UNAVAILABLE : '');
      $url = Url::fromUri($uri);
      $link = Link::fromTextAndUrl($title, $url)->toRenderable();
      $link['#attributes'] = [
        'title' => $this->t('Receive notifications about changes and/or comments to this page (and possibly similar pages).')
      ];
      $build = [];
      $build['link'] = $link;

      return $build;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    if ($node = $this->routeMatch->getParameter('node')) {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $node->id()]);
    }

    return parent::getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['route']);
  }

  /**
   * Checks if any other module wants to provide the UI.
   *
   * @param \Drupal\node\NodeInterface|null $node
   *   (optional) A node object. Defaults to NULL.
   *
   * @return bool
   *   Returns TRUE if no other module will provide the UI, FALSE otherwise.
   */
  private function canSubscribe(?NodeInterface $node = NULL): bool {
    $user = $this->currentUser;
    if (!$user->isAuthenticated() || empty($node)) {
      return FALSE;
    }
    $permission = $this->moduleHandler
      ->invokeAll('subscriptions_ui_get_permission_to_handle', [$node]);
    $permission = array_filter($permission);
    if (!empty($permission)) {
      return FALSE;
    }

    return TRUE;
  }
}
