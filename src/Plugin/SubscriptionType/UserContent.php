<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a subscription type to subscribe to user's node content.
 *
 * @SubscriptionType(
 *   id = "user",
 *   label = @Translation("User"),
 *   type = "node",
 *   field = "node_uid",
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node",
 *       label = @Translation("Node"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class UserContent extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return "subscribe to user's content";
  }

}
