<?php

namespace Drupal\subscriptions_content\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the API controller for Subscription content.
 *
 * @package Drupal\subscriptionContent\Controller
 *   Controller class.
 *
 * phpcs:enable
 */
class SubscriptionContent extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Drupal\Core\Config\ConfigManagerInterface definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Current request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructor for subscriptionContent.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   Current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object for current request.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager,
    AccountProxy $current_user,
    ConfigFactoryInterface $config_factory,
    Request $request
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('subscriptions_content.settings');
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * Handles the /api/subscription/content/notifications/settings endpoint. This
   * returns the notifications configuration settings on the project posting
   * a comment.
   *
   * GET:   "$host/api/subscription/content/notifications/settings",
   *   Get the notification settings on the project requesting things from.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function notificationsSettings(): JsonResponse {
    switch ($this->request->getMethod()) {
      // Fetch comments index by source id.
      case 'GET':
        // Fetch comments.
        return $this->subscriptionSettings();

      default:
                return new JsonResponse([
          'status' => 'Not Implemented',
          'error' => 'This type of request is not implemented',
        ]);
    }
  }

  /**
   * Handles the /api/subscription/content/notifications/settings endpoint. This
   * returns the notifications configuration settings on the project posting
   * a comment.
   *
   * GET:   "$host/api/subscription/content/notifications/settings",
   *   Get the notification settings on the project requesting things from.
   *
   * @param string $source_id
   *   Base64 HTML encoded source ID for the tracker entity. If it starts with
   *   dc: it is a dublin core ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function getSourceNode(string $source_id): JsonResponse {
    switch ($this->request->getMethod()) {
      // Fetch comments index by source id.
      case 'GET':
        // Fetch comments.
        return $this->getNodeBySourceId($source_id);

      default:
                return new JsonResponse([
          'status' => 'Not Implemented',
          'error' => 'This type of request is not implemented',
        ]);
    }
  }

  /**
   * Fetches data based on hashed source ID and returns API response.
   *
   * Comments index call
   * GET:/api/subscription/content/notifications/{source_id}?subscription_notify=1|0
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The returned JSON response.
   */
  public function subscriptionSettings(): JsonResponse {
    // Get this projects configuration settings.
    $config = (object) $this->config->get('subscriptions_content.settings')->getRawData();

    if (!empty($config)) {
      // Build out a config JSON response to return.
      $response = [
        'status' => Response::HTTP_OK,
        'method' => 'GET',
        'data' => $config,
      ];
    }
    else {
      // Error happened inform the user.
      $response = [
        'status' => Response::HTTP_BAD_REQUEST,
        'method' => 'GET',
        'data' => 'Config for subscriptions_content.settings could not be found.',
      ];
    }


    return new JsonResponse($response);
  }

  /**
   * Fetches data based on hashed source ID and returns API response.
   *
   * Comments index call
   * GET:/api/subscription/content/notifications/{source_id}?subscription_notify=1|0
   *
   * @param string $source_id
   *   Base64 HTML encoded source ID for the tracker entity. If it starts with
   *   dc: it is a dublin core ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The returned JSON response.
   */
  public function getNodeBySourceId(string $source_id): JsonResponse {

    // Find out if source ID is base64 encoded.
    // This will let developers test the end point easier without having to
    // encode things.
    if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $source_id)) {
      // Source ID was base64 encoded. Decode it now.
      $source_id = $this->base64UrlDecode($source_id);
    }

    // @todo: Fix this call and figure out how to use entityTypeManager, replacing some of the code below.
    /*    $comment_query = $this->entityTypeManager
          ->getStorage('comment')
          ->getQuery();

        // We can assume a single nid since we constrain field_red_hat_comments_source_id to be unique.
        $nid = $comment_query->condition('field_red_hat_comments_source_id', $source_id)
          ->execute();*/

    // Fetch the node from the source ID.
    $connection = \Drupal::database();
    $nid = $connection->select('node__field_red_hat_comments', 'rhc')
      ->fields('rhc', ['entity_id'])
      ->condition('field_red_hat_comments_source_id', $source_id)
      ->execute()->fetchField();

    if (!is_null($nid)) {
      // Load the node the comments live on.
      $node = $this->entityTypeManager->getStorage('node')->load($nid);

      // Convert all field data into an array for sending via json.
      foreach ($node->getFields() as $name => $field) {
        $myFields[$name] = $field->getString();
      }

      // Build out a response to return.
      $response = [
        'status' => Response::HTTP_OK,
        'method' => 'GET',
        'data' => $myFields,
      ];
    }
    else {
      // Build out a response to return.
      $response = [
        'status' => Response::HTTP_BAD_REQUEST,
        'method' => 'GET',
        'data' => 'Source ID was not able to be found.',
      ];
    }


    return new JsonResponse($response);
  }

  /**
   * Decode an html encoded base64 string.
   *
   * @param string $input
   *   Base64 string to convert.
   *
   * @return false|string
   *   FALSE on failure, string on success.
   */
  protected function base64UrlDecode(string $input) {
    return base64_decode(strtr($input, '._-', '+/='));
  }
}
