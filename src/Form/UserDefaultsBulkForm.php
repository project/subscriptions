<?php

namespace Drupal\subscriptions\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Stub for user default bulk operations form.
 */
class UserDefaultsBulkForm extends FormBase {

  /**
   * Private temporary storage.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStorePrivate;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): UserDefaultsBulkForm {
    return new static(
      $container->get('tempstore.private'),
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $private_temp_store_factory
   *   The private temporary storage service.
   */
  public function __construct(PrivateTempStoreFactory $private_temp_store_factory) {
    $this->tempStorePrivate = $private_temp_store_factory->get('subscriptions');
  }

  /**
   * Checks access to the form.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result object.
   */
  public function access(AccountInterface $account): AccessResultInterface {
    $has_permission = $account->hasPermission('bulk-administer user subscriptions');
    $has_session = !empty($this->tempStorePrivate->get('bulk_op'));

    return AccessResult::allowedIf($has_permission && $has_session);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriptions_user_defaults_bulk';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
