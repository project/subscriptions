<?php

namespace Drupal\subscriptions\Plugin\SubscriptionType;

/**
 * Provides a node type subscription type.
 *
 * @SubscriptionType(
 *   id = "node_type",
 *   label = @Translation("Content type"),
 *   type = "node",
 *   field = "type",
 *   context_definitions = {
 *     "node_type" = @ContextDefinition("entity:node_type",
 *       label = @Translation("Node type"),
 *       required = TRUE,
 *     ),
 *   }
 * )
 */
class NodeType extends SubscriptionTypePermissionAccessBase {

  /**
   * {@inheritdoc}
   */
  protected function getPermission(): string {
    return 'subscribe to content types';
  }

}
