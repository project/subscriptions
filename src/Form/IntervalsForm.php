<?php

namespace Drupal\subscriptions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the intervals settings form.
 */
class IntervalsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscriptions_intervals';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('subscriptions.config');

    $form['intervals'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Intervals'),
      '#description' => $this->t(
        'Each interval is defined by a single line in the format
        <em>seconds|label</em>. Adding a new interval is simple, but removing an
        interval that is already in use is not recommended.'
      ),
      '#placeholder' => "1|As soon as possible\n900|Every 15 minutes\n3600|Hourly\n10800|Every three hours\n86400|Daily",
      '#default_value' => $config->get('intervals') ?? '',
      '#element_validate' => [
        [
          $this,
          'validateIntervals',
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form element validation callback.
   */
  public function validateIntervals($element, FormStateInterface $form_state, $form) {
    // Get the form value.
    $intervals = explode("\n", $form_state->cleanValues()
      ->getValue('intervals', ''));
    $intervals = array_filter($intervals);

    // Regex for matching seconds|label values.
    $pattern = '/^([0-9]+)\|(.+)$/';
    $errors = [];
    // Check each line individually.
    foreach ($intervals as &$interval) {
      // Trim whitespace off of each line.
      $interval = trim($interval);
      if (empty($interval)) {
        continue;
      }
      $match = preg_match($pattern, $interval);
      // If the line did not match the regex, add it as an error.
      if ($match != TRUE) {
        $errors[] = $interval;
      }
    }
    // If any errors built up, set an error message.
    if (!empty($errors)) {
      $error_message = $this->formatPlural(
        count($errors),
        'Invalid format: @interval',
        'Invalid formats: @interval',
        [
          '@interval' => implode(', ', $errors),
        ]
      );
      $form_state->setError($element, $error_message);
    }
    // Clean up the final value and set it back in the form state.
    $intervals = array_values(array_filter($intervals));
    $form_state->setValueForElement($element, implode("\n", $intervals));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set values from the form.
    $form_state->cleanValues();
    $config = $this->config('subscriptions.config');
    $config->set('intervals', $form_state->getValue('intervals', ''));
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['subscriptions.config'];
  }

}
